#include "rotate.h"
#include <malloc.h>

struct image rotate(struct image const source) {
    if (source.data == NULL) {
        return (struct image) {0};
    }
    struct pixel *new_data = (struct pixel *) malloc(source.width * source.height * sizeof(struct pixel));

    for (uint64_t h = 0; h < source.height; h++)
        for (uint64_t w = 0; w < source.width; w++)
            new_data[(w * source.height) + h] =
                    source.data[((source.height - 1 - h) * source.width) + w];

    return (struct image) {.width = source.height, .height = source.width, .data = new_data};
}


