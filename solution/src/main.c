#include "bmb.h"
#include "img.h"
#include "rotate.h"
#include "util.h"

int main(int argc, char **argv) {

    if (argc < 3) {
        print_error("Wrong number of arguments");
        return -1;
    }

    FILE *input_file = fopen(argv[1], "rb");
    if (!input_file) {
        print_error("Bad first input file");
        return -1;
    }
    FILE *output_file = fopen(argv[2], "wb");
    if (!output_file) {
        fclose(input_file);
        print_error("Bad second input file");
        return -1;
    }

    struct image image;
    if (from_bmp(input_file, &image)) {
        print_error("Error converting from bmp");
        return -1;
    }

    struct image new_image = rotate(image);
    image_free(&image);
    if (to_bmp(output_file, &new_image)) {
        print_error("Error converting to bmp");
        return -1;
    }

    if (fclose(input_file) || fclose(output_file)) {
        print_error("Error closing file");
        return -1;
    }
    image_free(&new_image);
    return 0;
}
