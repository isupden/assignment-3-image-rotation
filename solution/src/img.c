#include "img.h"
#include <malloc.h>


struct image create_image(const uint64_t height, const uint64_t width) {
    return (struct image) {.height=height, .width=width, .data=malloc(height * width * sizeof(struct pixel))};
}


void image_free(struct image *img) {
    free(img->data);
}
