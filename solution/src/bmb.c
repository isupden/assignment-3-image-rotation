#include "bmb.h"
#include "img.h"
#include <stdint.h>
#define BMP_TYPE 19778
#define BMP_SIZE 40
#define BMP_PIXEL_PER_METER 2834
#define BMP_BIT_COUNT 24
#define BLOCK_WITH 4


static uint8_t get_padding(uint32_t width);

static int read_header(FILE *file, struct bmp_header *header);

static int read_content(FILE *file, struct image *image);

static int write_header(struct image image, FILE *file);

static int write_content(struct image image, FILE *file);

static const struct bmp_header header_blank = {
        .type = BMP_TYPE,
        .reserved = 0,
        .offset = sizeof(struct bmp_header),
        .headerSize = BMP_SIZE,
        .planes = 1,
        .compression = 0,
        .pixelPerMeterX = BMP_PIXEL_PER_METER,
        .pixelPerMeterY = BMP_PIXEL_PER_METER,
        .colorsUsed = 0,
        .colorsImportant = 0,
        .bit_count = BMP_BIT_COUNT
};


uint8_t get_padding(const uint32_t width) {
    uint8_t padding = (width * sizeof(struct pixel)) % BLOCK_WITH;
    return padding ? BLOCK_WITH - padding : padding;
}

int read_header(FILE *const file, struct bmp_header *const header) {
    return fread(header, sizeof(struct bmp_header), 1, file) != 1;
}

int read_content(FILE *file, struct image *const image) {
    const uint8_t padding = get_padding(image->width);
    struct pixel *content = image->data;
    for (uint64_t i = 0; i < image->height; i++) {
        if (fread(content + image->width * i, sizeof(struct pixel), image->width, file) != image->width) return 1;
        if (fseek(file, padding, SEEK_CUR) != 0) return 1;
    }
    return 0;
}

enum read_status from_bmp(FILE *const in, struct image *read) {
    struct bmp_header bmp_header = {0};
    if (read_header(in, &bmp_header)) {
        return READ_ERROR;
    }
    *read = create_image(bmp_header.height, bmp_header.width);
    if (read_content(in, read)) {
        return READ_ERROR;
    }
    return READ_OK;
}

int write_header(struct image image, FILE *const file) {
    struct bmp_header header = header_blank;
    size_t image_size = image.height * image.width * sizeof(struct pixel);
    header.imageSize = image_size;
    header.height = image.height;
    header.width = image.width;
    header.fileSize = sizeof(struct bmp_header) + image_size;
    return fwrite(&header, sizeof(struct bmp_header), 1, file) != 1;
}

int write_content(struct image image, FILE *const file) {
    const uint8_t padding = get_padding(image.width);
    const uint8_t paddings[3] = {0};
    for (size_t i = 0; i < image.height; i++) {
        if (fwrite(image.data + i * image.width, sizeof(struct pixel) * image.width, 1, file) == 0) return 1;
        if ((fwrite(paddings, padding, 1, file) == 0) && padding != 0) return 1;
    }
    return 0;
}

enum write_status to_bmp(FILE *out, struct image const *img) {
    if (write_header(*img, out))
        return WRITE_ERROR;
    if (write_content(*img, out))
        return WRITE_ERROR;
    return WRITE_OK;
}




