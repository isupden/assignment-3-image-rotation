#include "util.h"

void print_error(const char *const message) {
    fprintf(stderr, "%s", message);
    printf("%s", "\n");
}
