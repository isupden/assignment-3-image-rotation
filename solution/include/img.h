#ifndef IMAGE_H
#define IMAGE_H
#include <stdint.h>

#pragma pack(push, 1)

struct image {
    uint64_t width, height;
    struct pixel *data;
};
#pragma pack(pop)

struct pixel {
    uint8_t b, g, r;
};

struct image create_image(const uint64_t height, const uint64_t width);

void image_free(struct image *img);

#endif

