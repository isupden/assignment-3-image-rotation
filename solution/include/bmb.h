#ifndef BMP_H
#define BMP_H

#pragma pack(push, 1)

#include "img.h"
#include <stdint.h>
#include <stdio.h>

struct bmp_header
{
    uint16_t type;
    uint32_t fileSize;
    uint32_t reserved;
    uint32_t offset;
    uint32_t headerSize;
    uint32_t width;
    uint32_t height;
    uint16_t planes;
    uint16_t bit_count;
    uint32_t compression;
    uint32_t imageSize;
    uint32_t pixelPerMeterX;
    uint32_t pixelPerMeterY;
    uint32_t colorsUsed;
    uint32_t colorsImportant;
};
#pragma pack(pop)

enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_UNSUPPORTED_BITS,
    READ_ERROR
};

enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR
};

enum read_status from_bmp(FILE* in, struct image* const read);

enum write_status to_bmp(FILE* out, struct image const* img);


#endif

